'use strict';

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        username:   DataTypes.STRING,
        password:   DataTypes.STRING,
        email:      DataTypes.STRING,
        bio:        DataTypes.TEXT,
        avatar:     DataTypes.STRING,
        website:    DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                User.hasMany(models.Artwork)
            }
        }
    });

    return User;
};
