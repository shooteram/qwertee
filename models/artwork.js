'use strict';

module.exports = function(sequelize, DataTypes) {
    var Artwork = sequelize.define('Artwork', {
        UserId:     DataTypes.INTEGER,
        like:       DataTypes.INTEGER,
        favorite:   DataTypes.INTEGER,
        url:        DataTypes.STRING,
        title:      DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                Artwork.belongsTo(models.User, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
            }
        }
    });

    return Artwork;
};
