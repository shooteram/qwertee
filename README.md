# Qwertee

## Build Setup

``` bash
# install dependencies
npm install
```

#### A Database connexion is required
You're gonna need to install the driver that suits your needs
``` bash
npm install pg pg-hstore
npm install mysql # For both mysql and mariadb dialects
npm install sqlite3
npm install tedious # MSSQL
```
> I still recommend using _Mysql_ for the test purpose because of a `RAND()` SQL raw command which is not supported by _Sqlite3_.

Then, you need to configure the file `config/config.json`.
> If you're lost, here is the link to the _Sequelize_ documentation. [Sequelize Documentation](http://docs.sequelizejs.com/en/v3/)

When it's done, type this and wait.
``` bash
npm run prod # if you're on a Linux environment
npm run prod:windows # if you're on Windows
```
This command triggers the build of the _production_ version of the Single Page Application.
Then, it drop the database, populates it and seeds it with random data.
And the express server launch itself on the port 3000.

Next time, you don't want to go to that process all over again if there has been no changes to the frontend.
To launch the express server, type:
``` bash
npm run express # if you're on a Linux environment
npm run express:windows # if you're on Windows
```
