var models  = require('../models');
var Sequelize  = require('../models').sequelize;
var express = require('express');
var router = express.Router();

router.get('/top', function (req, res) {
    models.Artwork.findAll({ order: [['like', 'DESC'],] , limit: 3, include: [ models.User ] })
        .then( function(artwork) {
            res.json(artwork)
        })
});

router.get('/top/old', function (req, res) {
    models.Artwork.findAll({ order: [[ 'like', 'DESC' ]], offset: 3, limit: 3, include: [ models.User ] })
        .then( function(artwork) {
            res.json(artwork)
        })
});

router.get('/random', function (req, res) {
    models.Artwork.find({ order: [ Sequelize.fn( 'RAND' ) ], include: [ models.User ] })
        .then( function(artwork) {
            res.json(artwork)
        })
});

router.post('/user', function (req, res) {
    models.User.find({ where: { username: req.query.username }, include: [ models.Artwork ] })
        .then( function(user) {
            res.json(user)
        })
});

router.post('/artwork', function (req, res, next) {
    models.Artwork.find({ where: { id: req.query.id } })
        .then( function(user) {
            res.json(user)
        })
});

module.exports = router;
