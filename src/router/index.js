import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import * as Index from 'components'
import * as User from 'components/User'

Vue.use(VueRouter)
Vue.use(VueResource)

Vue.http.options.emulateJSON = true;

export default new VueRouter({
    // mode: 'history',
    base: __dirname,
    linkActiveClass: "is-active",
    routes: [
        { path: '/', meta: { title: 'Home' }, name: 'index', component: Index.Home },
        { path: '/random', meta: { title: 'Random' }, name: 'random', component: Index.Random },
        { path: '/blog', meta: { title: 'Blog' }, name: 'blog', component: Index.Blog },
        { path: '/forum', meta: { title: 'Forum' }, name: 'forum', component: Index.Forum },
        { path: '/About', meta: { title: 'About' }, name: 'about', component: Index.About },
        {
            path: '/user/:username',
            component: User.Main,
            children: [
                { path: '', meta: { title: 'User Profile' }, name: 'user', component: User.Home },
                { path: 'artwork/:artwork', meta: { title: 'User Artwork' }, name: 'artwork', component: User.Artwork }
            ]
        }
    ]
})
