import Vue from 'vue'
import App from './App'
import router from './router'

router.beforeEach((to, from, next) => {
  document.title = 'Qwertee - ' + to.meta.title
  next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
