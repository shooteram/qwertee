import Home from './Home.vue'
import About from './About.vue'
import Forum from './Forum.vue'
import Blog from './Blog.vue'
import Random from './Random.vue'

export { Home, About, Forum, Blog, Random }
