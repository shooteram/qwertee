import Main from './Main.vue'
import Home from './Home.vue'
import Artwork from './Artwork.vue'

export { Main, Home, Artwork }
