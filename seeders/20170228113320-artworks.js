'use strict';
var faker = require('faker');
var qwerteeTees = require('../qwertee-image.json');

module.exports = {
    up: function (queryInterface, Sequelize) {
        var fakeArtworks = [];

        for (var i = 0; i < 100; i++) {
            fakeArtworks.push({
                UserId:     faker.random.number({ min: 1, max: 100 }),
                like:       faker.random.number({ min: 2, max: 3500 }),
                favorite:   faker.random.number({ min: 1, max: 2500 }),
                url:        faker.random.arrayElement(qwerteeTees),
                title:      faker.random.words(),
                createdAt:  new Date(Date.now()),
                updatedAt:  new Date(Date.now())
            });
        }

        return queryInterface.bulkInsert('Artworks', fakeArtworks, {});
    },

    down: function (queryInterface, Sequelize) {}
};
