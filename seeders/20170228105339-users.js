'use strict';
var faker = require('faker');

module.exports = {
    up: function (queryInterface, Sequelize) {
        var fakeUsers = [];

        for (var i = 0; i < 100; i++) {
            fakeUsers.push({
                username:   faker.internet.userName(),
                password:   faker.internet.password(),
                email:      faker.internet.email(),
                bio:        faker.lorem.text(),
                website:    faker.internet.url(),
                avatar:     faker.image.avatar(),
                createdAt:  new Date(Date.now()),
                updatedAt:  new Date(Date.now())
            });
        }

        return queryInterface.bulkInsert('Users', fakeUsers, {});
    },

    down: function (queryInterface, Sequelize) {}
};
